---
title: Verdere uitwerking
subtitle: Week 4 | Woensdag
tags: ["concept", "SC", "feedback"]
categories: ["conceptvorming"]
date: 2017-09-27
---

De dag is opgestart met enkele raadsels uit het spel 'Black Stories'. Hierbij is er een spelleider en de rest van het team moet vragen stellen om erachter te komen wat er is gebeurd, of te wel... Wij activeren onze hersenen. Het viel op dat we hierdoor sneller actief werden. Ihsan was er het begin van de dag niet i.v.m. een afspraak met de tandarts en het wegbrengen van zijn broertjes en Rowie kon niet komen, omdat er plots geen treinen meer bleken te rijden. We volgden de studiodag dus met z'n drieën.

Ik stelde voor om aan de slag te gaan en het volgende te doen:
We hadden allen ons eigen idee over een idee dat al stond wat we hebben uitgetypt in een document. Vervolgens hebben we elkaars ideeën aangevuld om zo een overzicht te krijgen. Hierna hebben we enkele elementen samengevoegd tot een nieuw concept wat er daarna verder zijn gaan doordenken. Zo hebben we iets meer details ingevuld aan het plan om een concreter concept te krijgen.

Ik heb afgesloten met een studiecoach bijeenkomst, waarbij ik dit keer als voorzitter optrad in de opdracht van  deze week om het groepje van vier te leiden. Als feedback kreeg ik te horen dat het goed gelukt is iedereens aandacht erbij te houden en de opdracht te verduidelijken.  
