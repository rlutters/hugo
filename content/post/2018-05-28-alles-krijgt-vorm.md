---
title: Alles krijgt vorm!
subtitle: Week 5 | Maandag
tags: ["prototype", "expo", "plattegrond"]
categories: ["high fid"]
date: 2018-05-28
---
Vandaag hebben we de poster en plattegrond uitgeprint, samen met een aantal foodtrucks om te zien of alles zou gaan werken zoals gepland. Het knutselen neemt veel tijd in beslag. We hebben gekeken met papiertjes waar de foodtrucks op de plattegrond zouden komen te staan en vervolgens de foodtrucks naar die schaal aangepast. De rest van de foodtrucks doen we later, we gaan morgen eerst testen in Afrikaanderwijk 