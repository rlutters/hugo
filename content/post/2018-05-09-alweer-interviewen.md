---
title: Alweer interviewen
subtitle: Week 2 | Woensdag
tags: ["interviews", "Afrikaanderbuurt"]
categories: ["onderzoeken"]
date: 2018-05-09
---
We deden weer een nieuwe poging om te gaan interviewen in Afrikaanderbuurt. We zijn de doelgroep meer benadert met nieuwe vragen en deze keer is het beter gelukt om terug te komen met goede interviews. We hebben zowat de hele dag rondgelopen op zoek naar de doelgroep om zo interviews te verkrijgen.  