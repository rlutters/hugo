---
title: Prototype V2
subtitle: Week 7 | Maandag
tags: ["feedback", "low-fid prototype", "leerdossier"]
categories: ["prototype"]
date: 2018-03-26
---
Een groot deel van de ochtend is besteeds van het laten voorzien van feedback op onze inzichtenkaarten door projectdocent Elske. Daarna hebben we het concept verder uitgedacht en uitgediept. Hier heb ik vervolgens een contentoverzicht van gemaakt en conceptbeschrijving. Ook heb ik een tweede versie van mijn conceptposter gemaakt. Daarna heb ik zelfstandig gewerkt aan een nieuw low-fid prototype voor het concept en deze afgerond. 