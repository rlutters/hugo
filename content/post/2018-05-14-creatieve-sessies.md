---
title: Creatieve sessies
subtitle: Week 3 | Maandag
tags: ["COCD-box", "Creatieve sessie"]
categories: ["ideevorming"]
date: 2018-05-14
---
Deze dag hebben we ingericht met een creatieve sessie en verschillende creatieve technieken. Dit begon met het noemen van al bestaande ideeën. Vervolgens hebben we nog zelf ideeën bedacht en bestaande ideeën uitgediept. Met een COCD-box hebben we vervolgens geconvergeerd en d.m.v. stiekem stemmen is er uiteindelijk een idee uitgekomen. 