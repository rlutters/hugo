---
title: The Journey begins!
subtitle: Week 2 | Woensdag
categories: ["onderzoek"]
tags: ["userjourney", "enquête"]
date: 2017-11-22
---

Vandaag zijn we begonnen met het maken van een enquête om meer inzicht te krijgen in onze doelgroep. We hebben enkele vragen bedacht puur gericht op het uitgaansleven. Wat zorgt ervoor dat mensen terug komen? Wat spreekt je aan in een club? Wat is er voor nodig om te club te adviseren bij je vrienden? 

Ook  hebben we als team een begin gemaakt aan de User Journey (huidige situatie), we hebben hiervoor gekeken naar enkele voorbeelden van andere user journeys om te zien hoe deze zijn opgebouwd. Vervolgens hebben we een indeling gemaakt waar we ons aan kunnen houden, een chronologische volgorde van situaties. Ik keek naar welke touchpoints er zijn en vervolgens hebben we aan de hand van de enquête gekeken wat de kansen zijn en welke activiteiten men doorloopt tijdens een bezoek aan PAARD.