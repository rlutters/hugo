---
title: Creatieve technieken
subtitle: Week 6 | Maandag
tags: ["creatieve techniek", "met de klok mee", "waanzinnige ideeën", "stiekem stemmen"]
categories: ["conceptvorming"]
date: 2018-03-19
---
Vandaag hebben we gewerkt aan enkele creatieve technieken vanuit een workshop. Deze namen nog vrij veel tijd in beslag, maar waren voor mij wel nieuw en dus handig om een keer te gebruiken. 
Zo hebben we een eigen voertuig moeten bouwen, bedachten we eigen problemen en waanzinnige oplossingen daarbij om vervolgens met de klok mee in te zetten en een COCD-box te gebruiken om te convergeren om zo stiekem te stemmen over de laatste ideeën die overbleven. Zo zijn we gekomen bij ons nieuwe concept.