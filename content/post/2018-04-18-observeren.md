---
title: Observeren!
subtitle: Week 10 | Woensdag
tags: ["observatie", "fotocollage", "afrikaanderbuurt"]
categories: ["onderzoeken"]
date: 2018-04-18
---

Met het hele team zijn we naar Afrikaanderbuurt gegaan om daar de omgeving en onze doelgroep te observeren. Gelijktijdig maken we foto's voor een fotocollage om de omgeving daar te kunnen analyseren. We hebben een dag uitgekozen waar er een markt op het plein staat, omdat we aannamen dat we op zo'n moment de doelgroep sneller zouden treffen en beter konden vinden. Dit is uiteindelijk niet echt gelukt. Wel hebben we een goede fotocollage kunnen maken.