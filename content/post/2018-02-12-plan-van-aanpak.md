---
title: Plan van Aanpak
subtitle: Week 2 | Maandag
tags: ["plan van aanpak", "stakeholder map"]
categories: ["teamvorming"]
date: 2018-02-12
---


Blijkbaar zijn er al groepen gemaakt. Teamcaptains werden opgeroepen om te controleren of de groepen in orde waren en of er niets raars aan de hand was. Wat bleek? Ik zit nu in een team van 4, waaronder twee oude teamgenoten. Dit zorgt ervoor dat het team al beter op elkaar is ingewerkt, maar ook weer nieuwe inzichten heeft. 

Ik moest voor mezelf duidelijk krijgen wat nu de opdracht precies was en daarvoor een plan van aanpak maken met het team, dus dit heb ik gedaan en daarnaast heb ik er ook een stakeholder map bij gemaakt om aan te geven wie alle betrokkenen zijn in het hele verhaal.