---
title: Weer een kick-off
subtitle: Week 9 | Maandag 
tags: ["kick-off"]
categories: ["kick-off"]
date: 2018-04-09
---

Vandaag hadden we weer een kick-off met Joke Mulder, de opdracht werd even snel herhaald en er werd ons verteld uit welke wijken van Rotterdam Zuid we konden kiezen. We hebben gekozen voor Afrikaanderbuurt. Vervolgens ben ik begonnen met het maken van een Plan van Aanpak voor de rest van de periode. 