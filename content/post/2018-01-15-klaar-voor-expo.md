---
title: Staat alles klaar voor de expo?
subtitle: Week 8 | Maandag
tags: ["expo", "conceptposter"]
categories: ["presentatie"]
date: 2018-01-15
---
We hebben besloten om vandaag de taken weer op te splitsen. Ik ga met een ander teamlid aan de slag met de conceptposter die we op de expo kunnen tonen, terwijl de rest aan de slag gaat met het maken van de kleine bordjes (ons product) voor op de tafel. Alle benodigheden worden alvast uitgeprint, zodat we dat niet kunnen vergeten. Ook heb ik nog enkele visuele dingetjes aangepast aan de schermen zodat het net iets professioneler overkomt. De bordjes zijn ingekleurd en ik heb het schermpje ervoor gemaakt en erop gemonteerd. Alles zou klaar moeten zijn voor die expo van woensdag.