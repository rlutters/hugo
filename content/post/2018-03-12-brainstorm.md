---
title: Brainstorm sessies
subtitle: Week 5 | Maandag
tags: ["brainstorm", "creatieve techniek", "negatieve brainstorm", "mindmap", "onwerkelijke ideeën", "sharing stories", "feedback", "workshop", "data visualisatie"]
categories: ["conceptvorming"]
date: 2018-03-12
---


Deze dag hebben gewerkt aan ideevorming. Ieder teamlid heeft zijn eigen soort ideevormingtechniek gedaan. Zo hebben we sharing stories gedaan, ik heb een negatieve brainstorm verzorgd, we hebben nog een mindmap gemaakt en nagedacht over onwerkelijke ideeën. Dit heeft aardig wat tijd in beslag genomen, maar zo kwamen we wel op een groot scala aan ideeën. Ik heb om feedback gevraagd aan Bob vanwege mijn lifestyle diary. Het bleek dat ik emoties miste die ik nog moest toevoegen. Wel was mijn diary logisch en chronologisch ingedeeld. 

's Middags ben ik naar de workshop 'Data visualisatie' geweest van Mieke.