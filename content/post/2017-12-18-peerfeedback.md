---
title: Appels met peerfeedback vergelijken
subtitle: Week 6 | Maandag
tags: ["peerfeedback", "testplan", "validatie", "recap"]
categories: ["testen"]
date: 2017-12-18
---
We kregen deze ochtend weer formulieren om peerfeedback te noteren voor elkaar. Dit maakt eigenlijk duidelijk hoe anderen vinden dat je functioneert in de groep en wat je eventuele verbeterpunten zijn of wat je juist moet blijven doen omdat dat goed gaat. Je krijgt feedback van drie teamleden en moet vervolgens kijken of alle gegeven feedback duidelijk is en hoe ze het bedoelen. 

Daarna hebben we snel het testplan aangepast voor het nieuw concept en zijn naar buiten gegaan om te testen, maar dit heeft niet heel veel resultaat opgeleverd. Uiteindelijk zijn we terug naar binnen gegaan want daar waren nog validaties voor het low-fid prototype waar ik naartoe ben gegaan. Andere leden hebben de recap laten valideren. Vanwege lange wachttijden, nam dit de hele middag in beslag.