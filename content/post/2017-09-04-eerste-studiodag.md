---
title: De eerste studiodag!
subtitle: Week 1 | Dag 1
categories: ["Inventarisatie"]
tags: ["introductieweek", "teamposter", "doelen", "planning"] 
date: 2017-09-04
---

Na de introductieweek, gaan we nu met ons samengesteld team *Mozaique* de studio in. De eerste opdracht; het aanstellen van een teamleider en de [inventarisatie](https://hrnl.sharepoint.com/sites/Mozaique/Shared%20Documents/Game%20voor%20Rotterdam/Deliverables/Inventaricatie%20.pdf) van het team opstellen. Iedereen heeft zijn eigen kwaliteiten en achtergrond kenbaar gemaakt. Zo ben ik structureel, creatief en doelgericht. Afgelopen jaar VWO afgerond en nu door met CMD. We hebben gekeken wat de ambities zijn van het team zoals een goed teamverband en ons gestelde doel behalen.

Deze kwaliteiten en ambities konden wij gelijk kwijt op onze teamposter, de volgende opdracht. Nadat ik voor mijzelf een aantal schetsjes gemaakt had, kwam een ander met een idee waar heel het team direct enthousiast voor werd. Mijn 'mooie' handschrift werd ingezet voor de tekst op de poster.

Tegelijk ook onze doelen voor het team opgesteld samen met de regels waar elk teamlid zich aan moet houden.

> Geef het aan als je later bent, zodat de rest van het team hiervan op de hoogte is.

Nu gaan we daadwerkelijk aan de slag met de opdracht. Het punt waar het team zich opsplits. Dit is de week van het onderzoeken en dus hebben we ervoor gekozen om de verschillende onderzoeksvragen op te delen in het team.

Ik zelf ben verantwoordelijk geweest voor het vinden van vaak terugkerende thema's in Rotterdam. Het team liet zich inspireren door de tour van afgelopen week en zodoende heb ik nog wat extra Rotterdamse bijnamen opgezocht voor gebouwen en/of monumenten.

De eerste dag is afgerond met het maken van een planning, waar ikzelf veel inspraak heb gehad. Ik heb mij er, om het zo te zeggen, flink mee bemoeid - samen met de teamleider - terwijl de rest van het team nog even doorging met onderzoeken.
