---
title: Peerfeedback
subtitle: Week 2 | Maandag
tags: ["peerfeedback", "interviews", "voorbereiden"]
categories: ["onderzoeken"]
date: 2018-05-07
---
Deze dag zijn we begonnen met het organiseren van peerfeedback, om dit vervolgens weer te bespreken en er iets mee te doen. We hebben besloten om het interviewen weer opnieuw te proberen en hebben de vragen weer wat aangepast om gerichter onderzoek te doen. 