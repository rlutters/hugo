---
title: Kwartaalreflectie
subtitle: Kwartaal 3
tags: ["kwartaalreflectie", "kwartaal 3", "EMI"]
categories: ["kwartaalreflectie"] 
date: 2018-03-30
---

Dit kwartaal startte hier in school met een kick-off door onder andere Joke Mulder. De dagen daarna stond voor ons een programma klaar in het StayOkay hostel in Maastricht. Hier hebben we een soort introducerende opdracht gedaan voor het thema van dit kwartaal. Het gaat over gezondheid en dan vooral de gezondheid in Rotterdam Zuid. Daarvoor moeten we een concept gaan bedenken om een gezonde levensstijl te stimuleren. De opdracht wordt gesplitst in twee delen. Eerst focussen we op de studenten van de Hogeschool Rotterdam, in het tweede deel draait 't om Rotterdam Zuid zelf. 

We moesten beginnen met het maken van een plan van aanpak. Alvorens we startte met het werk, moest duidelijk zijn welke methodes je wilt gaan inzetten. Ook hebben we gekozen om gebruik te maken van een SCRUM-board. Hierna zijn we na wat validaties aan de slag gegaan met het proces. We zijn gestart met wat deskresearch en het maken van een moodboard. Daarna zijn we aan de slag gegaan met het opstellen van interview vragen om vervolgens per team lid twee mensen te interviewen. Van al het onderzoek hebben we een inzichtenkaart gemaakt om resultaten op een rij te krijgen. 

Daarna hebben we heel wat verschillende creatieve technieken ingezet, ieder van ons minstens één. Hierdoor hebben we een veelheid van ideeën gekregen en deze vervolgens d.m.v. een COCD-box en stiekem stemmen geconvergeerd. 

Nadat we een idee hadden, heb ik hier een contentoverzicht van gemaakt en heb ik gewerkt aan een storyboard, wireframes en low-fid prototypes. Hier heb ik vervolgens feedback op gevraagd en ik ben naar validaties geweest. Hier heb ik een tweede versie van gemaakt voor een nieuw idee dat we hadden aan de hand van een nieuw idee dat we kregen uit andere creatieve technieken. 

Daarna moest er een leerdossier komen en moest ik me gaan voorbereiden op een tentamen. Volgend kwartaal ga ik met hetzelfde team focussen op de doelgroep in Rotterdam Zuid en zullen we het concept daarop aan moeten passen. 