---
title: Valideren maar
subtitle: Week 9 | Woensdag
tags: ["validatie", "plan van aanpak"]
categories: ["kick-off"]
date: 2018-04-11
---

Ik heb vandaag het Plan van Aanpak afgemaakt en laten controleren door mijn teamgenoten. Vervolgens heb ik deze bij Elske laten valideren, dit is gelukt. Hierna zijn we aan de slag gegaan met het eerste op de planning. Het Plan van Aanpak moest nog wel ietsje aangepast worden om goed door te kunnen, dus dat is ook nog gebeurd. Ook is er een begin gemaakt aan het onderzoeksplan. 