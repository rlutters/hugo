---
title: Spelanalyse
subtitle: Week 5 | Dinsdag
categories: ["testen"]
tags: ["prototype", "personal challenge", "spelanalyse"]
date: 2017-10-03
---

Vandaag is de laatste dag voor de presentie. Normaal niet onze derde studiodag, maar voor deze week eventjes wel, omdat we het anders gewoon niet redden.
Aidan is verdergegaan met het prototype dat vrij snel af was. Ik ben zelf verder gegaan met mijn personal challenge. Mijn individuele taken waren al klaar en ik kon de groep verder ook niet echt helpen, maar ik sprong bij waar nodig. 

Vervolgens moesten we de spelanalyse ging doen. Hierbij wachtte iedereen maar rustig af. We vroegen een team, maar dat duurde nog een kwartier dus het team besloot te wachten, maar dat liep al uit naar een halfuur. Ik heb, omdat ik op het moment niets anders te doen had, ook geholpen bij andere spelanalyses. Hierbij kon ik ook zien hoe andere dit doen en hoe wij onze methode konden verbeteren voor een betere spelanalyse en een duidelijker resultaat.

Dan de spelanalyse zelf, het liep een beetje rommelig. We hadden ook niet echt afgesproken hoe we het aan zouden pakken. Ons team was opgesplitst in twee, maar die communicatie liep niet zo heel lekker. Uiteindelijk ehhebn we wel gewoon goede feedback gehad waar we ook zeker iets aan hebben dus daar gaan we mee aan de slag.