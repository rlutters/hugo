---
title: De motivatieladder op
subtitle: Week 6 | Dinsdag
tags: ["motivatieladder", "reflectie", "samenwerken", "SC"]
categories: ["samenwerken"]
date: 2018-03-20
---
Na nog wat gewerkt te hebben aan het project na de SC bijeenkomst, stelde ik voor een reflectie te maken op de samenvatting, mede vanuit de workshop 'Samenwerken' van Elbert. Na deze reflectie heb ik mijn teamleden de motivatieladder laten invullen. Voor mij zelf hielp dat en ik hoopte dat dat bij hen ook het geval was. Hiermee probeerde ik mijn doel te behalen om mijn teamleden goed te kunnen motiveren. 