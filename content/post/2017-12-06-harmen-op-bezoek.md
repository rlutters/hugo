---
title: Harmen op bezoek
subtitle: Week 4 | Woensdag
tags: ["opdrachtgever", "pitch", "low-fid prototype"]
categories: ["presentatie"]
date: 2017-12-06
---
Om een uur of elf zou Harmen langs komen om onze pitches en concepten te evalueren. De laatste voorbereidingen aan de pitch worden gelegd en vanaf dat moment is het eigenlijk wachten op Harmen. Ondertussen heb ik via POP snel een ingescande versie gemaakt van het low-fid prototype zodat hier interactie op mobiel kon ontstaan. We analyseerde nog heel even de resultaten van het de enquête van gisteren. Toen Harmen langs kwam, bleek ons idee heel erg te lijken op de ideeën van anderen. Bovendien zou het concept niet het probleem oplossen, dus konden we helemaal opnieuw beginnen. Hierdoor raakten we nogal gedemotiveerd en hadden we even de tijd nodig om onszelf te herpakken.