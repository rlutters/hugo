---
title: Laatste studiodag
subtitle: Week 5 | Woensdag
tags: ["prototype", "expo", "afronding"]
categories: ["high fid"]
date: 2018-05-30
---
Gister hebben we getest, dus vandaag zijn we aan de slag gegaan met de testrapportage. Hierna hebben we het high-fid prototype afgerond door de rest van de foodtrucks in elkaar te knutselen en de juiste positie te geven. Vandaag is onze laatste studiodag dus we hebben ervoor gezorgd dat alles klaarstaat voor de expo, verder hebben we nog enkele dingen afgerond.