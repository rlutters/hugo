---
title: Kwartaalreflectie
subtitle: Kwartaal 4
categories: ["kwartaalreflectie"]
date: 2018-06-01
---

Dit kwartaal begon met weer een kick-off, dit keer gaan we door op vorig kwartaal. Na de competentietafels, zijn er nog enige compenties te halen en daar zal aan gewerkt moeten worden. Het begon met het kiezen van een wijk in Rotterdam Zuid. Dit kwartaal was ik verantwoordelijk voor het Plan van Aanpak. Dit is in één keer gevalideerd, het moest nog wel een beetje worden aangepast. Vervolgens is er gestart aan een onderzoeksplan. Toen heb ik samen met Lorenzo de ontwerpproceskaart gemaakt, om nogmaals het proces goed in kaart te brengen. 

Daarna begonnen de observaties in Afrikaanderwijk. We zijn die wijk vaker binnengetreden om onderzoek te doen naar de doelgroep en het concept. Ook hebben we een fotocollage gemaakt om de omgeving beter in beeld te krijgen. Tussentijds hebben we op school de vragen voorbereid voor het interviewen. Steeds was er moeite met het vinden van de doelgroep. Hier hebben we later advies voor geraadpleegd zodat we toch nog doorkonden.

Er is weer peerfeedback opgenomen op de teamleden. We hebben opnieuw een poging gedaan tot interviewen. Hierna is er opnieuw een creatieve sessie geweest om ideeën en concepten te kunnen vormen. Er is gebruikt gemaakt van al bestaande ideeën, deze origineel maken en vervolgens zelf ideeën bedenken. Er is geconvergeerd met een COCD-box. 

We hebben allen een storyboard gemaakt om het uiteindelijk idee vorm te geven en toen werd het tijd voor het testplan. We waren bang dat we weer de doelgroep niet konden vinden en in verband met de tijdsnood, zijn we alvast begonnen met het invullen van het high-fid prototype en de expo. Later zijn we gaan testen en is er een testrapportage gemaakt. Het high-fid prototype met in elkaar geknutselde foodtrucks is afgerond en staat volledig klaar voor de expo. 