---
title: Waarnemend teamleider
subtitle: Week 2 | Maandag
tags: ["voorzitter", "planning", "paperprototype", "presentatie"]
categories: ["prototype"]
date: 2017-09-11
---
Vandaag bleek onze teamleider niet aanwezig te zijn en hij gaf mij de taak om hem waar te nemen. Ik heb vervolgens gelijk geïnventariseerd hoever iedereen is met zijn of haar moodboard en visualisatie. Dit bleek wat tegen te vallen, maar toen ik vervolgens aan gaf hoe we het vandaag zouden moeten aanpakken, ging iedereen hier in mee en gelijk aan de slag.

Ik had het idee dat alles even recht getrokken moest worden om terug op de rails te komen om het zo te zeggen. Nu was het niet zo erg zoals wellicht beschreven, maar het leek mij makkelijk een overzicht te hebben van waar wij nu staan. Er is afgesproken dat iedereen zijn individuele taken zelf afmaakt. Onderzoeksresultaten zijn goed besproken.

We zijn met z'n allen het concept verder gaan uitwerken, om vervolgens te beginnen aan het paperprototype. Deze is zo goed als afgerond vandaag. Morgen maken wij de schermen af en gaan we werken aan de presentatie. We maken gebruik van een kartonnen model van een smartphone om te presenteren. Hiervan willen wij dan een opname maken.

Ik was vandaag vooral verantwoordelijk voor het boeken van vooruitgang met het team, het aansturen hiervan en zorgen dat iedereen erbij bleef met zijn gedachten. Ik wil er graag voor zorgen dat het op ieder moment voor iedereen duidelijk is wat er moet gebeuren, zodat we niet achterlopen. Met de presentatie op woensdag is er namelijk een krapper schema. Aan het eind van deze dag liggen wij op schema.
