---
title: Alle taken op een rij
subtitle: Week 7 | Maandag
tags: ["to-do list", "user journey", "high-fid prototype", "schermontwerp"]
categories: ["prototype"]
date: 2018-01-08
---
Allereerst hebben we na de kerstvakantie een lijstje gemaakt met alles wat nog gemaakt en gedaan moest worden. Dit leek in eerste instantie veel maar bleek wel mee te vallen. Er moest een user journey komen voor de toekomstige situatie. Deze taak heb ik op me genomen. Ook moest het high-fid prototype af, hiervan heb ik ook het grootste deel van de visuele schermen gemaakt en ontwerpen afgaande op de huisstijl van PAARD. 