---
title: Puntjes op de I
subtitle: Week 2 | Donderdag
categories: ["prototype"]
tags: ["personal challenge", "onderzoeksrapport", "presentatie"]
date: 2017-09-14
---

De laatste studiodag van de week. Begonnen met een Personal Challenge van Illustrator. Ik kan nog niet zeggen dat ik echt iets nieuws heb geleerd, aangezien de basics vandaag behandeld zijn en ik al een beetje m'n weg kan vinden in het programma.

Vandaag was er een duidelijke taakverdeling in het team om de laatste deliverables af te kunnen ronden. Mijn taken waren al zo goed als afgerond. Ik heb het prototype nog gefilmd en heb nog wat veranderingen gemaakt aan mijn deel van het onderzoeksrapport. Ook nam ik een klein beetje de rol over van het verdelen van taken, aangezien dat vandaag niet uit de teamleider leek te komen.

Verder raakt het team steeds meer op elkaar ingespeeld. Het wordt steeds gezelliger en er wordt steeds meer en serieuzer werk verricht. Ik voel me zeer prettig bij deze vorderingen. Ook zijn er afspraken gemaakt voor dé presentatie van maandag, ons laatste feedback moment voor iteratie 1.