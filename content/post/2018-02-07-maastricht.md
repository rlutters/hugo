---
title: StayOkay in Maastricht
subtitle: Week 1 | Woensdag t/m vrijdag
tags: ["kennismaking", "busreis", "teamvorming", "concept", "BRAVO-model"]
categories: ["kick-off"]
date: 2018-02-07
---
Dan gaan we met de bus op weg naar Maastricht, een reis van zo'n 3 uur. In het begin was het nog maar de vraag waarvoor precies. Het blijkt dat we een soort mini kwartaal houden daar met een inleidende opdracht voor de komende twee kwartalen wat betreffende de gezonde leefstijl. 

Eerst moesten we een avatar en masker maken, een soort alternatieve identiteit van mijzelf. Hier is voor de rest niets gedaan, behalve een random samenstelling van groepen selecteren. Zo kwam je dus terecht in een groep van mensen die je verder totaal niet kent. We moesten Maastricht in om studenten te gaan ondervragen over hun ontspanning in combinatie met alcohol en roken. De volgende dag moesten we een simpel concept bedenken wat deze studenten zou sturen richting een gezonde leefstijl. We hebben met dit concept geen prijs gewonnen en zijn vervolgens weer naar huis gegaan.