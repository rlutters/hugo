---
title: Afsluiting conceptfase
subtitle: Week 5 | Maandag
categories: ["conceptvorming"]
tags: ["convergeren", "voorbereiding"]
date: 2017-10-02
---

Vandaag moesten we toch echt een concept gaan kiezen, ondanks de feedback van Bob. Ik constateerde dat we te lang bleven hangen in het bedenken van ideeëen, maar er was niemand die de knoop doorhakte, dus ik besloot er iets aan te doen. Ik stelde steeds een vraag met twee keuzes, we stemden en zo kwamen we dan toch tot één idee waar we mee verder konden. Gezien de tijdsdruk moesten we wel verder. Vanaf nu konden we beginnen aan het prototype en ik besloot dat het voor nu wellicht het makkelijkste was om dit digitaal te doen. Dan konden we snel dingen aanpassen en kregen we direct een goed beeld van hoe alles eruit kwam te zien. Tijdens het maken van het prototype zijn de details ingevuld, zodat alles in één keer een veel duidelijkere vorm kreeg. 

Samen met Sico heb ik een voorbeeld route gemaakt, waarbij ik moest kijken welke locaties  in aanmerking kwamen gezien de bekendheid, locatie en afstand ten opzichte van de andere locaties. Deze route zullen we ook in de presentatie plaatsen, zodat alles iets duidelijker wordt. Morgen gaan we verder met het prototype en kunnne we de spelanalyse doen.