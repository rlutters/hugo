---
title: Design Theory - Het ontwerpproces
subtitle: Week 1 | Dinsdag
tags: ["theorie", "hoorcollege"]
date: 2017-09-05
---

> **Design** is to **design** a **design** to produce a **design**.

Een definitie die mij vandaag is bijgebleven bij het hoorcollege. Waarom ik dit nog even specifiek benadruk, is omdat het mij op een andere manier heeft laten kijken naar het designen. Het is belangrijk om te leren ontwerpen vanuit de doelgroep en niet van wat je zelf mooi vindt overkomen en waarvan je denkt dat het werkt.

> **Design** is not **art**.

De komende tijd zal ik dus gaan leren om een huidige situatie om te gaan zetten in een gewenste situatie en ik hoop dat proces vast te kunnen leggen in deze blog.
