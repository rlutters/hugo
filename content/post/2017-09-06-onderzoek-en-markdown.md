---
title: Onderzoek en markdown
subtitle: Week 1 | Woensdag
categories: ["onderzoek"]
tags: ["deskresearch", "planning", "workshop", "blog"]
date: 2017-09-06
---
Ik had het gevoel dat het team vandaag nog even wakker moest worden. We wachtte op de verder informatie die we zouden krijgen en zijn vervolgens door gegaan met het onderzoek. Het bleek dat we nog enkele onderzoeksvragen hadden openstaan en dus heb ik onderzoek gedaan naar *hoe je mensen kunt verbinden met elkaar*. Wat ik eigenlijk voornamelijk tegenkwam is dat men daarvoor veel tijd met elkaar moet doorbrengen, overeenkomsten met elkaar moeten vinden en elkaar moeten kunnen vertrouwen.

Wat ik zelf een mooi idee vond:
> Vreemden willen het onbekende aankijken en samen naar de antwoorden zoeken

Veel eerstejaarsstudenten, onze doelgroep, kennen elkaar nog niet en zijn dus vreemden voor elkaar. Het lijkt mij dus een goed idee om die studenten te laten zoeken naar antwoorden. Verder kwamen de termen *passie, waarde en structuur* naar boven. Vooral ook *competitie* zal een zeer belangrijke rol spelen. Het is nu uitzoeken hoe we dit kunnen toepassen in het spel.

Ook heb ik deze dag de planning even voor iedereen overzichtelijk genoteerd op onze site - de Outlook Groep op OneDrive - zodat iedereen daar nu toegang toe heeft en dit kan inzien. Ook zal ik iets bijwerken zodra het is afgerond.

Dan de workshop '*Een blog schrijven*'. Ik dacht uitleg te krijgen over hoe wij dit blog al online zetten, maar blijkbaar is dat een stap voor later. Ik moet voorlopig alleen gebruik maken van *markdown*, terwijl ik al gewend ben om met HTML te werken, dus ik sla deze stap liever over, maar ik zie wel in wat het nut van markdown is. Bovendien typt het een stuk sneller, dus heb ik er geen probleem mee.

De dag werd afgesloten door een vergadering met onze studiecoach. Na afloop weet ik nog niet helemaal wat ik ermee ben opgeschoten, maar misschien dat dat later duidelijk wordt. Wel is duidelijk geworden welke vaardigheden ik graag zou willen leren, wat neer kwam op het delen van m'n energie - mede om ook mezelf omhoog te trekken op bepaalde (moeilijke) momenten.
