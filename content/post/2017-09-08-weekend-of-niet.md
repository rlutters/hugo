---
title: Weekend of niet?
subtitle: Week 1 | Vrijdag/Zaterdag
tags: ["visualiseren", "moodboard"]
categories: ["conceptvorming"]
date: 2017-09-08
---

Het weekend is nu begonnen, maar ik kan niet stilzitten. Ik moet mijn visualisatie afmaken samen met het moodboard. Ik heb nog nooit een moodboard gemaakt, dus ik moet nog even uitzoeken wat daar precies van verwacht wordt, maar ik heb een vermoeden.

-------

Een andere taak voor dit weekend is om te werken aan eigen concepten voor de game. I.p.v. het concept dat wij al hadden staan, wil ik het gaan proberen met gebouwen. Het is in feiten hetzelfde, maar dan gezien vanuit een andere invalshoek. Ik zal dit maandag bespreken met de rest van het team om te kijken  wat hun er van vinden.
