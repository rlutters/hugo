---
title: Knippen en plakken
subtitle: Week 4 | Woensdag
tags: ["lifestyle diary", "feedback", "validatie", "workshop", "deskresearch", "samenwerken"]
categories: ["reflectie"]
date: 2018-03-07
---

De lifestyle diary moet bestaan uit geplakte afbeeldingen en wat tekst. Het moet heel visueel zijn. Thuis heb ik de meeste plaatjes al op mijn vel geplakt, dus ik heb vandaag wat feedback gevraagd aan Bob. Mijn emoties missen en die moeten erbij. Contact met mensen wordt niet duidelijk, maar dat hoeft er niet per sé bij. Ook heb ik zelf bedacht om er nog wat inzichten bij te zetten. We zijn emoties gaan uitprinten en die heb ik verder op mijn blad geplakt, om de diary compleet te maken. De validatie is volgende week. Mijn lifestyle diary is besproken in de groep en we hebben wat gebrainstormt betreffende het concept. 

In de middag heb ik de workshop Deskresearch bijgewoond waarbij ik wat handige tips kreeg om te zoeken op Google en de informatie over hoe de online mediatheek werkt. Daarna ben ik nog naar de workshop samenwerken geweest, waar ik geleerd heb inzicht te krijgen in hoe de samenwerking verloopt en wat je eraan kunt doen om het te verbeteren. 