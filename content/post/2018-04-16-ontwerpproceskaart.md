---
title: De ontwerpproceskaart
subtitle: Week 10 | Maandag
tags: ["ontwerpproceskaart"]
categories: ["planning"]
date: 2018-04-16
---

Deze dag heb ik gewerkt aan een ontwerpproceskaart aan de hand van het plan van aanpak om het hele proces wat duidelijker in kaart te brengen. Hiervoor is er weer gebruikt gemaakt van de standaard formulieren waarbij we de processen hebben uitgeknipt en deze op een blad hebben geplakt in de juiste volgorde. 