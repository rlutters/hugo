---
title: Meer low-fid prototype!
subtitle: Week 5 | Woensdag
tags: ["low-fid prototype", "concept"]
categories: ["prototype"]
date: 2017-12-13
---


Ieder van ons heeft eerst een eigen low-fid prototype gemaakt met een eigen interpretatie van het nieuwe concept dat we hadden liggen. Zo krijgen we van iedereen duidelijk te zien hoe die persoon erover denkt. Vervolgens hebben we allerlei ideeën en suggesties samen gevoegd tot een nieuw low-fid prototype waarin van iedereen wel wat terug te vinden was. Daarnaast is er een start gemaakt aan de recap van de afgelopen weken. Hiermee liepen we een beetje achter.