---
title: MSE en écht van start
subtitle: Week 3 | Maandag
tags: ["SC", "NSE", "plan van aanpak", "validatie", "leerdossier", "brainstorm", "enquête", "interview"]
categories: ["inrichten ontwerpproces"]
date: 2018-02-19
---

We maken kennis met onze nieuwe studiecoach Mieke die ons de laatste twee kwartalen zal coachen. Hiervoor moesten we een formuliertje invullen, zodat ze ons beter zou leren kennen. Daarnaast moesten we de nationale studenten enquête invullen, de NSE. Hierdoor wordt de Hogeschool Rotterdam beoordeelt op de kwaliteiten van het onderwijs en ondersteuning. 

Wat betreft het project is vandaag onze nieuwe versie van het plan van aanpak gevalideerd door Elske. Het plan is verbeterd, concreter en duidelijker maar nog steeds niet concreet genoeg voor een voldoende. Wel mogen we het plan van aanpak nu opnemen in ons leerdossier. Hierna hebben we nog even gebrainstormt voor vragen die we konden stellen tijdens een enquête of interview. 