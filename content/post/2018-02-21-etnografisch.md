---
title: Etnografisch aan de slag
subtitle: Week 3 | Woensdag
tags: ["ontwerpproceskaart", "methodes", "workshop", "etnograpisch onderzoek"]
categories: ["inrichten ontwerpproces"]
date: 2018-02-21
---


We hebben gewerkt aan een ontwerpproceskaart, een cirkel met alle stadia en welke methodes we willen gebruiken. We noemen het ons design proces. In de middag heb ik een workshop bijgewoond 'Etnografisch onderzoek' met Joke Mulder en Micha. Het begon met een presentatie over wat etnografisch onderzoek precies is en werd afgesloten met een opdracht. Hiervoor moest ik alles wat ik m'n tas zat uitstorten op een A3 vel, dit logisch indelen en vervolgens omtrekken. Hierna moest ik met iemand anders de inzichten uit mijn vel en inhoud van de tas. Ik wilde nog een workshop volgen, maar daar kon ik mij niet voor inschrijven, dus daar mocht ik niet heen. 