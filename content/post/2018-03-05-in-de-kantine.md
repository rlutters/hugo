---
title: In de kantine
subtitle: Week 4 | Maandag
tags: ["presentatie", "ontwerpproceskaart", "inzichtenkaart", "brainstorm", "observeren"]
categories: ["onderzoek"]
date: 2018-03-05
---

De ochtend begon met een presentatie van Elske en Bob, ik moest na de vakantie nog een beetje opstarten. Na de teamcaptainmeeting kregen we bladen mee. Dit waren een soort templates voor een ontwerpproces kaart, maar ook voor inzichtskaarten. Hier kan men het resultaat op beschrijven wat blijkt uit onderzoek. We hebben een korte brainstorm gedaan over hypotheses die volgens ons zouden spelen in de huidige situatie.

In de pauze zijn we gaan zitten in de kantine en The Greenhouse om mensen te observeren. We hebben precies bijgehouden hoeveel mensen een gezond broodje of ongezonde broodje kopen. Ditzelfde geldt voor drank. Ik heb bijgehouden hoeveel mensen er op hun mobiel en/of laptop zitten. Ook is er bij gehouden hoe mensen zich gedroegen, denk aan het twijfelen bij aankoop bijvoorbeeld. 
