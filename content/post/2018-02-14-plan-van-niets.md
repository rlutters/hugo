---
title: Plan van niets
subtitle: Week 2 | Woensdag
tags: ["presentatie", "validatie", "plan van aanpak", "stakeholders map", "deskresearch", "moodboard"]
categories: ["inrichten ontwerpproces"]
date: 2018-02-14
---

In de ochtend hadden we de presentatie van ons plan van aanpak voor de projectdocenten. Aansluitend zou deze direct gevalideerd worden. Hiervoor hebben wij een onvoldoende ontvangen. Het plan is niet concreet genoeg en de planning ontbreekt eigenlijk net als veel andere zaken. Mijn stakeholders map was wel goed als ik een paar kleine aanpassingen zou maken.

In de middag zijn we verder gegaan met het plan van aanpak en hebben wij het uitgediept. Ik ben ook alvast begonnen met deskresearch voor mijn moodboard dat ik maandag af moet hebben.