---
title: Werkcollege en onderzoek
subtitle: Week 1 | Donderdag
tags: ["enquête", "visualiseren", "werkcollege"]
categories: ["onderzoek"]
date: 2017-09-07
---
Vandaag de dag begonnen met enkele potjes tafelvoetbal. Ik ben van mening dat het doen van dit soort competitieve zaken het teamverband versterkt, waardoor we beter tot ons recht komen als team. Ook kan ik nu beter en directer serieus aan de slag.

We hebben vervolgens gewerkt aan de enquête, waarbij ik verantwoordelijk was voor enkele vragen en inspraak heb gehad op de opmaak. Deze hebben wij uitgeprint en vervolgens uitgedeeld onder verschillende CMD'ers.

Vervolgens heb ik een begin gemaakt aan het visualiseren van mijn onderzoek, '*Hoe verbind je mensen met elkaar?*'.
Ook heb ik de planning hier en daar bijgewerkt, omdat blijkt dat enkele taken zijn verschoven.

Vervolgens heb ik het werkcollege gevolgd, waarin ik de theorie van afgelopen dinsdag kon verwerken. Ik moest een situatie uit mijn leven terughalen waarin ik een destijds huidige situatie heb omgezet in een gewenste situatie. Dan blijkt opeens dat je het ontwerpproces al veel vaker hebt doorlopen dan je zou denken. Daarna hebben we gekozen voor één van de vijf processen om deze verder uit te werken en dit daarna te presenteren.
