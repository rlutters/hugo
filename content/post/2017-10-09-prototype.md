---
title: Prototype 2.0
subtitle: Week 6 | Maandag
categories: ["prototype"]
tags: ["concept", "presentatie", "expo"]
date: 2017-10-09
---

En de derde iteratie is gestart die natuurlijk weer begint met een briefing. Deze keer moeten we kijken naar de kern van het concept. We moeten het simpel houden, maar toch verder uitwerken of te wel de kern uitdiepen. We hebben het een en ander besproken om het concept verder te verduidelijken, want dit was volgens de presentatie nog al vaag.

Ik heb vandaag een tweede versie van het prototype gemaakt en helemaal uitgewerkt met alle meldingen en interacties. Het is nu ook mogelijk om het team te kiezen waarmee je samen wilt werken, wat ook in de rest van de app te zien is en klopt. Alleen het gedeelte van de voicechat is nog niet interactief. Vervolgens heb ik samen met Sico gekeken naar het invullen van de rest van de kaarten, zodat die alvast klaar zijn voor de expo. 