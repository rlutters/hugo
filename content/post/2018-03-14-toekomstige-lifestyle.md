---
title: Toekomstige lifestyle
subtitle: Week 5 | Woensdag
tags: ["lifestyle diary", "feedback", "validatie", "workshop"]
categories: ["onderzoek"]
date: 2018-03-14
---


De ochtend begon met een aantal mededelingen, daarna zijn we direct onze persoonlijke lifestyle diaries gaan vergelijken. We hebben hier feedback omgevraagd, ik had deze al eerder verkregen bij Bob. Daarna hebben we gewerkt aan de toekomstige lifestyle diary. Ik heb de afbeeldingen opgezocht voor ontspanning en de icoontjes ervoor. We hebben feedback gevraagd over hoe we de toekomstige lifestyle diary konden opstellen en gaan aan de hand daarvan aan de slag. Ook hebben we een plan uitgestippeld voor de verdere conceptontwikkeling, zo willen we een conceptposter, low-fid prototype, wireframes en een UX storyboard maken per persoon.

s'Middags ben ik naar de validatie geweest van mijn lifestyle diary, welke vervolgens beoordeeld is als voldoende om in mijn leerdossier op te nemen. Daarna heb ik nog een workshop gevolgd bij Elbert. Dit was de workshop Eigenaarsschap waar ik geleerd heb om op te gaan met motivatie.