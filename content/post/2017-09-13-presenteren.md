---
title: Presenteren van paperprototype
subtitle: Week 2 | Woensdag
tags: ["planning", "paperprototype", "feedback", "spelanalyse", "workshop", "SC"]
categories: ["prototype"]
date: 2017-09-13
---

De dag begon met wat informatie en planning voor de dag. Iedere groep mocht zijn paperprototype presenteren om daar vervolgens feedback op te mogen ontvangen. Zo bleken enkele zaken nog niet helemaal te kloppen. De feedback die ik heb genoteerd, zullen wij gaan verwerken in het nieuwe paperprototype. Hier gaan wij morgen mee verder.

Voor vandaag heb ik mij bezig gehouden met het opstellen van een verslag van hoe de game gespeelt wordt. Dit was ook mijn rol bij de spelanalyse die wij hebben gedaan vandaag. Ik zorgde ervoor dat alles duidelijk was voor de spelers, omdat er nog geen werkende instructies waren in beeld. Ook dit wordt in het nieuwe paperprototype verwerkt.

Het was al vrij snel tijd voor de workshop '*Beeldanalyse*'. Ik heb hier geleerd op een andere manier te kijken naar beelden en deze ook anders te interpreteren. Dit is zelfs geoefend met een zelfuitgekozen foto, maar voor mijn gevoel ging de workshop niet diep genoeg om hier nu al iets van toe te kunnen passen. Volgende week is er een vervolg, maar ik heb me al reeds ingeschreven bij een andere workshop op dat moment.

De dag is afgesloten met een bijeenkomst met de studiecoach. Ook leerde wij vandaag onze peercoach kennen en is mij duidelijk geworden waarmee en wanneer ik bij hem terecht kan. Altijd handig om te weten!
