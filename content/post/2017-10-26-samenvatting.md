---
title: Kwartaal 1
subtitle: Samenvatting en terugblik
categories: ["kwartaalreflectie"]
date: 2017-10-26
---

Zo, het is tijd om terug te blikken op afgelopen kwartaal. Wat heb ik geleerd? Wat ging goed, wat ging minder goed? Wat is beter gegaan? Nog één keer alles langs om te zien wat ik afgelopen weken geleerd heb.

**Iteratie 1**
Daar kom ik dan binnen in de nieuwe studio, ondertussen wel vertrouwd natuurlijk. Een team dat ik verder nog niet heel goed ken en dan het beste ervan zien te maken. Met die instelling kwam ik binnen. Ik mag zeggen dat ik best blij ben met het resultaat dat we als team hebben neergezet. Het begint natuurlijk met het maken van een teamposter, taken verdelen. Vervolgens mochten we binnen één week een heel onderzoek doen naar onze doelgroep. De werkdruk leek voor mij een moment heel hoog te liggen. Toch kan ik zeggen dat deze naderhand wat afnam. We konden het rustig aan doen, eigen pauzes inlassen zolang wij maar gewoon werk opleverde. En dat is zo goed als voor de deadlines gebeurd.

Dan natuurlijk de eerste hoorcollege's, voor mij totaal nieuw, maar eigenlijk gewoon te vergelijken met een les op het middelbare. Goed opletten en aantekeningen maken.

We mogen ook opeens met een blog gaan werken met markdown, waarom? Ik begrijp HTML prima, maar uiteindelijk typt het zo veel sneller. Ook met het blog is een hoop gedoe geweest, maar het is uiteindelijk allemaal opgelost gelukkig. 

Nog meer nieuwe dingen, een studiecoach. Wie is die man? Blijkbaar de persoon waar je overal voor bij terrecht kan en dit blijkt ook echt waar te zijn. Het is iemand die zich echt in zet voor je. Alleen jammer dat daarom heen ook zoveel onduidelijk was, maar daar blik ik later op terug.

Terug naar de design challenge. Find your Peercoach, toch een prima eerste idee leek ons en ik kreeg er zin in om het helemaal uit te gaan werken. Ik heb ook de teamleider een dagje moeten waarnemen, waarin ik eigenlijk zo veel mogelijk in probeerde te halen aangezien we voor mijn gevoel achter op schema liepen. Zo kreeg ik ook insight in hoe de meetings met alle teamcaptains verlopen en vind het handig om zo te zien hoe ver andere teams staan. Onder leiding van de teamcaptain spelanalyses gedaan, een paperprototype gemaakt - wat je een hoop werkt bespaart en het concept opeens helemaal niks blijkt te zijn - en dan het grote fenomeen *kill your darling*. Natuurlijk, waarom ook niet? Alles waar je de afgelopen drie weken aan gewerkt hebt als team mag je nu de vuilnisbak ingooien. Ik stond te juichen, maar niet heus.

**Iteratie 2**
Oké, er moet een nieuw plan komen, maar hoe? Creatieve techieken? Nooit iets mee gedaan. Workshops volgen dus. Aan de hand daarvan leerde ik hoe een brainstorm precies is opgezet en hoe deze ingezet kan worden. Dat is ook precies wat ik gedaan heb. De eerst volgende studiodag heb ik een brainstorm voorgesteld om zo snel mogelijk nieuwe ideeën te krijgen met het team en met resultaat mag ik wel zeggen. We wilde iets gaan doen met audio, maar wat precies? Dit is uiteindelijk vertaald in communiceren d.m.v. voiceclips. Hierna bleven we heel lang hangen in ideeën. Ik verzamelde van iedereen de ideeën, plaatste deze in een bestand, liet teamleden dingen toevoegen bij elkaar om zo uiteindelijk het concept te kiezen waarvoor we zouden gaan. Nadat we een concept hadden, toch feedback gevraagd en toen konden we zo goed als opnieuw beginnen. Dit was best een frusterende periode, ideeën waren ook niet voor iedereen duidelijk. Toch uiteindelijk tot een concept gekomen, dit in sneltreinvaart uitgewerkt, want de presentatie voor de alumni kwam er al aan. Deze heb ik samen met Sico gepresenteerd, met de presenatie was niet veel mis. De volgorde was goed en het verhaal was duidelijk te volgen. Echter zaten er nog wel een aantal gaten in het concept.

**Iteratie 3**
Deze gaten moeten nu gedicht worden, maar dit bleek niet zo makkelijk te zijn als we dachten. We wilden vooral de eenvoud behouden, maar om de gaten op te vullen kwamen wij met allemaal losse elementen wat ten koste zou gaan van de eenvoud. Om deze eenvoud toch te behouden, hebben we er uiteindelijk unaniem voor gekomen om enkele gaten open te laten. Om dit op te lossen hadden we meer tijd nodig gehad en die tijd was er simpelweg niet. Wel heb ik een revisie gemaakt van het prototype, met meerdere schermen, meer interacties en een betere/duidelijkere speluitleg, zodat iedereen aan de hand van het prototype het spel direct zou begrijpen. 

Met de expo die hierop volgde, hebben we helaas geen prijs gewonnen, maar gelukkig wel de ervaring van een expo op kunnen doen. Deze had ik zelf namelijk nog nooit meegemaakt. Je moet meerdere malen een pitch zouden om jouw spelidee enthousiast over te dragen, maar je moet ook kritisch gaan kijken naar andere spellen. Af en toe dacht ik zelfs 'Waarom heb ik daar niet aan gedacht?'. Het is heel apart om te zien hoeveel creatieve ideeën er dan wel niet zijn.

Dan het hele gedoe omtrent het leerdossier. Blijkbaar zittenwe in een muterende opleiding. Ik als student wil graag concrete informatie over wat er nu precies van mij verwacht wordt. Als er dingen zijn die ik zelf moet invullen, wil ik wel weten dat dat de bedoeling is. In dit specifieke geval gaan de veranderingen zo snel dat de studiecoach die ik toch als aanspreekpunt zie de veranderingen niet kan bijbenen en dus met andere informatie komt dan bijvoorbeeld de projectdocent. In dat geval weet ik niet waar ik mij aan moet houden. Gelukkig ging de studiecoach hier wel achter aan en is het allemaal netjes uitgezocht, maar ik zou deze informatie ook graag eerder willen hebben, indien mogelijk. Het is voor nu allemaal nog zeer kortdag. Maar, ik ga er vanuit dat dat hoort bij een nieuwe opleiding. Volgend kwartaal is er al vast een stuk meer duidelijker. 

Voorlopig heb ik het nog steeds naar m'n zin op de opleiding en ga ik vrolijk verder. Op naar kwartaal 2!