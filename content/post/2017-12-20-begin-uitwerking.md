---
title: Van low naar high-fid
subtitle: Weel 6 | Woensdag
tags: ["recap", "validatie", "reflectie", "schermontwerp", "styleguide"]
categories: ["prototype"]
date: 2017-12-20
---

Vandaag heb ik mij toegewijd aan het verbeteren van de recap. Deze is laatst niet gevalideerd. Er moest meer reflectie in op het gemaakte werk. We hebben aantrekkelijke zinnen bedacht voor op het startscherm van het product en zijn hierna begonnen met het maken van deze startschermen in de huisstijl en het kleurenschema van PAARD. Van te voren is daarvoor vandaag de visual styleguide gemaakt van de club. 