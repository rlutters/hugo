---
title: Hoe nu verder?
subtitle: Week 3 | Woensdag
tags: ["bespreken", "deadlines", "storyboard", "testplan"]
categories: ["inrichten ontwerpproces"]
date: 2018-05-16
---
Vandaag hebben we eerst alles op een rijtje gezet zodat we wisten hoe we verder moesten gaan. We hebben enkele deadlines opnieuw ingericht en aangepast naar de huidige situatie. We hebben allen ons storyboard besproken dat we gemaakt hebben, bijgehorend bij het concept om te zien hoe iedereen over het concept dacht en vervolgens is er een begin gemaakt aan het testplan, omdat we binnenkort moeten gaan testen. 