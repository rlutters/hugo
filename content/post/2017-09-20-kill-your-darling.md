---
title: Kill your darlings
subtitle: Week 3 | Woensdag
tags: ["enquête", "moodboard", "workshop", "brainstorm", "SC", "teamafspraken"]
categories: ["onderzoek"]
date: 2017-09-20
---

Deze dag startte ik met het maken van een nieuwe enquête voor de CMD studenten, om ververvolgens mijn moodboard te kunnen verbeteren. We moeten deze week verdiepend onderzoek gaan doen naar onze doelgroep. 

In de middag had ik de workshop 'Creatieve technieken' waar ik leerde om goed te brainstormen en dat ook in praktijk heb uitgevoerd. Dit moet mij leren om sneller met creatieve ideeën op tafel te komen. 
Hierna was er weer een bijeenkomst met de studiecoach waarbij we onze teamafspraken hebben bijgewerkt, wat hopelijk de werkhouding in de studio zal gaan verbeteren. Ook is iedereen akkoord gegaan aan de hand van een ritueel.