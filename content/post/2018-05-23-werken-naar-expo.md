---
title: Werken naar Expo
subtitle: Week 4 | Woensdag
tags: ["conceptposter", "expo"]
categories: ["high fid"]
date: 2018-05-23
---
Er is een begin gemaakt aan het high-fid prototype. Er moet nog getest worden, maar het high-fid is slechts een opzet en zal later echt worden uitgewerkt. We zijn gaan kijken naar referenties hoe je foodtrucks kon vouwen/knutselen. Ook zijn we recepten gaan zoeken om inhoud te geven aan het concept. Ik heb vandaag een conceptposter gemaakt, twee recepten opgezocht en bij elk recept een bijpassende foodtruck gemaakt.  