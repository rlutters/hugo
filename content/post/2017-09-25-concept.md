---
title: Conceptontwikkeling
subtitle: Week 4 | Maandag
tags: ["ontwerpproceskaart", "moodboard", "brainstorm", "enquête", "COCD-box"]
categories: ["onderzoek"]
date: 2017-09-25
---

De dag begon met het maken van een ontwerpkaart, één van de deliverables voor deze week, waarop we laten zien wat we in de eerste iteratie en de tweede (tot nu toe) hebben gedaan. Dit wordt weergegeven in stappen in het proces, zoals een moodboard, planning e.d. Hiermee kunnen we achteraf snel zien welke stappen er gezet zijn, waar er iets is fout gegaan en of we niet iets in de verkeerde volgorde hebben uitgevoerd.

Vervolgens hebben we de nieuwe enquête uitgeprint en deze uitgedeeld in de studio. Hieruit bleek dat humor zeker een element moeten worden in onze game, voor de rest zijn de interesses van de studenten vrij divers.

We zijn gaan brainstormen met het team onder mijn advies volgens de techniek zoals deze is uitgelegd in de workshop met de rood- en groen licht fase en vervolgens de COCD-box. We hebben gelijk een goede indruk van welke ideeën niet gaan werken en welke wel. We hebben uiteindelijk gekozen voor een concept met audio, iets wat ik heb kunnen inbrengen in de sessie. Het was mij opgevallen dat bij de afgelopen presentaties niemand iets deed met audio, terwijl hier juist heel veel potentie kan liggen. Er is een idee, maar een uitgewerkt concept is er nog niet en daar zullen we later over beslissen. Mijn idee voor nu is een hint voor een locatie o.i.d. verdelen in vijf stukken en ieder teamlid (een team bestaat uit vijf) een deel van deze hint laten horen. De leden moeten dan zelf uitzoeken in elke volgorde de delen moeten staan om de hint volledig te begrijpen. Hierdoor komt men gelijk met elkaar in contact en moet men gefocusd blijven.

